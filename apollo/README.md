# Client application
Welcome to Client side of web application created for bachelor theses.
Client application takes care of all user interactions with the app. 
It communicates with server using its API endpoints.  


More details about Client functions and processes are described in folowing
chapters.

Whole repository is also available on GitLab https://gitlab.fit.cvut.cz/svehlalb/apollo


## Requirements
For successful installation you need to first make sure your
computer fulfils following requirements:

* Node.js [Installation guide][https://nodesource.com/blog/installing-nodejs-tutorial-windows/]

The installation process isn't optimized so if you run into any problems
contact Albert Svehla via this email address: svehlab@cvut.cz
## Run
Steps for successful running of Client application:

1. In a directory 'client' double-click on file run.bat.
2. If you wish to install dependencies and mainly Vue.js write 'Y' to command line
   and hit enter
3. After installation process application should automatically run itself. If the process didn't end successfully
   check that you have installed all required extensions.
3. Client application runs at address http://localhost:8080/. 

##  Structure
Application files are organized in following structure:

````
├── public  ... all public files used in application 
     └── files ... folder mainly with images, 
                   also contains test images for variant 27 
├── src 
     └── assets
           └── sass ... all CSS files
     └── components ... all componets used by app
           └── editorComponents 
           └── formSections
           └── resultsComponents
     └── router ... folder responsible for inital set up of routing
     └── store ... folder responsible for vuex operations (saving data)
     └── views ... all views used by app
     └── App.vue
├── run.bat ... file with scripts for running application     
         

````

## Usability
As teacher, you have two main reasons for using this application – creating exam and evaluating exam. 

### Creating exam

1. For creating an exam open link [http://localhost:8080/form?exam=1][http://localhost:8080/form?exam=1]. The exam parameter
defines id of new exam project and it can be modified.
2. In the form, fill in the data about exam and continue to editor.
3. In editor, create exam variants and assign students to them. After editing click
on the continue button and wait until the PDF forms are generated (you must also run server application in background)
4. If the process ended with success, you'll be redirected to download page
5. In download page, you can download all the generated PDF files.

You can try dynamic features in the editor:
* adding or duplicating exam new variants
* adding variant sections (pages) by clicking between list items
* drag and drop item between lists
* equal distribution of students to groups (exam variants)
* deleting items in list or even whole list

### Evaluating exam
If you don't want to print, mark and scan the sheets you can use prescanned 
test variant described below. If you do, change the exam id parameter to yours.
1. For evaluating exam open link [http://localhost:8080/upload?exam=27][http://localhost:8080/upload?exam=27]
2. If you use prescanned variant open 'client/data' folder. There are two files: 'Zadání_varianta_1.pdf' and 
   'Zadání_varianta_2.pdf'. Upload these files and click on 'Opravit' button.
3. If you want to upload your files, create new PDF files that corresponds with 
   created exam variants. Use this structure: 'Zadání_varianta_{variant_id}.pdf'. Fill these PDF files with
   scanned images. Upload PDF files to application and click on 'Opravit' button.
4. After successful evaluation process, application redirects you to results page.
   You can check the results of each student and his answers. If you have chosen
   your own exam variants, you won't properly see the images.
 
[https://nodesource.com/blog/installing-nodejs-tutorial-windows/]: https://nodesource.com/blog/installing-nodejs-tutorial-windows/

[http://localhost:8080/form?exam=1]: http://localhost:8080/form?exam=1

[http://localhost:8080/upload?exam=27]: http://localhost:8080/upload?exam=27
