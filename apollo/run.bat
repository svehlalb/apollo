@echo off
set /p id="Do you wish to install dependencies : "

if "%id%"=="Y" (
    goto yes
) else (
    if "%id%"=="y" (
        goto yes
    ) else (
    goto no
    )
)

:yes
echo "%% INSTALLING Vue.js%%"
 call npm install --save vue

echo "%% INSTALLING DEPENDENCIES%%"

 echo "%% Installing @fortawesome/fontawesome-free %%"
 call npm install --save @fortawesome/fontawesome-free
 echo "%% Installing @fortawesome/fontawesome-svg-core %%"
 call npm install --save @fortawesome/fontawesome-svg-core
 echo "%% Installing @fortawesome/free-solid-svg-icons %%"
 call npm install --save @fortawesome/free-solid-svg-icons
 echo "%% Installing @fortawesome/vue-fontawesome %%"
 call npm install --save @fortawesome/vue-fontawesome
 echo "%% Installing axios %%"
 call npm install --save axios
 echo "%% Installing bootstrap %%"
 call npm install --save bootstrap
 echo "%% Installing bootstrap-vue %%"
 call npm install --save bootstrap-vue
 echo "%% Installing core-js %%"
 call npm install --save core-js
 echo "%% Installing vue-axios %%"
 call npm install --save vue-axios
 echo "%% Installing vue-router %%"
 call npm install --save vue-router
 echo "%% Installing vue-draggable %%"
 call npm install --save vue-draggable
 echo "%% Installing vuex %%"
 call npm install --save vuex

:no
 echo "Starting..."
    npm run serve
pause
