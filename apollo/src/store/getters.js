export default {
  /* Function returns question from userQuestions test data */
  getQuestion: (state) => (id) => {
    return state.userQuestions.content.find(x => x.id === id)
  },
  /* Function returns question from student test data */
  getStudent: (state) => (username) => {
    return state.studentsData.content[0].find(x => x.username === username)
  }
}
