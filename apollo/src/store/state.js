import form from '../data/form'
import userQuestions from '../data/userQuestions'
import studentsData from '../data/students'

export default {
  form,
  userQuestions,
  studentsData
}
