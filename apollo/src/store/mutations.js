export default {
  /* Function updates initial attributes in form object */
  updateFormData: (state, payload) => {
    state.form.name = payload.name
    state.form.description = payload.description
    state.form.layout = payload.layout
    state.form.studentGroup = payload.studentGroup
    state.form.id = payload.id
  },
  /* Function updates variant attribute in form object */
  updateVariantForm: (state, payload) => {
    state.form.variants = payload.variants
  }
}
