export default {
  /* Function adds new data to form object */
  addFormData: ({ commit }, payload) => {
    commit('updateFormData', payload)
  },
  /* Function adds new data about variant to form object */
  addVariant: ({ commit }, payload) => {
    commit('updateVariantForm', payload)
  }
}
