import Vue from 'vue'
// Import FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faTimes, faPlusCircle, faCircle, faPlus, faCheck, faTimesCircle, faExpandAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import './assets/sass/index.sass'

library.add(faSearch, faTimes, faPlusCircle, faCircle, faPlus, faCheck, faTimesCircle, faExpandAlt)
Vue.component('font-icon', FontAwesomeIcon)
