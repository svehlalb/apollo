/**    __Test data__
 * Questions with answers
 * Final structure can be different - see the
 * API design https://app.swaggerhub.com/apis/svehlalb/Apollo/1.0.0
 */

export default {
  content: [{
    id: 1,
    type: 'multiple',
    question: 'Jaká oblačnost a počasí se objeví při vlhké a nestabilní vzduchové hmotě, která je převládajícím větrem tlačena proti pohoří a nucena stoupat?',
    correct_answer: 'zastřené Cb s bouřkami a přeháňkami krup anebo deště',
    incorrect_answers: [
      'nevysoká, nestrukturovaná oblačnost Ns s mírným mrholením nebo sněžením (v zimě)',
      'nevysoká oblačnost typu As nebo Cs s mírnými a dlouhotrvajícími srážkami',
      'zataženo nízkým stratem (vysoká mlha) bez srážek'
    ],
    points: 1
  },
  {
    id: 2,
    type: 'multiple',
    question: 'Jaký typ mlhy vzniká, když je vlhký a téměř nasycený vzduch nucen převládajícím větrem stoupat do kopců nebo malých hor?',
    correct_answer: 'orografická mlha',
    incorrect_answers: [
      'radiační mlha',
      'advekční mlha',
      'mlha v kopcích'
    ],
    points: 1
  }, {
    id: 3,
    type: 'multiple',
    question: 'Jaká situace v meteorologii se nazývá přerůstání konvektivní oblačnosti?',
    correct_answer: 'vertikální rozvoj kumulů až do dešťových přeháněk',
    incorrect_answers: [
      'změna od termiky bez oblačnosti k termice s oblačností v odpoledních hodinách',
      'nárůst kumulovité oblačnosti až pod vrstvu inverze',
      'vývoj termické níže až k bouřkové níži'
    ],
    points: 1
  }, {
    id: 4,
    type: 'multiple',
    question: 'Jaké je složení vzduchu?',
    correct_answer: 'kyslík 21 %, dusík 78 %, vzácné plyny / kysličník uhličitý 1 %',
    incorrect_answers: [
      'dusík 21 %, kyslík 78 %, vzácné plyny / kysličník uhličitý 1 %',
      'kyslík 78 %, vodní pára 21 %, dusík 1 %',
      'kyslík 21 %, vodní pára 78 %, vzácné plyny / kysličník uhličitý 1 %'
    ],
    points: 1
  }, {
    id: 5,
    type: 'multiple',
    question: 'Ve které vrstvě atmosféry dochází nejčastěji k meteorologickým jevům?',
    correct_answer: 'troposféra',
    incorrect_answers: [
      'stratosféra',
      'tropopauza',
      'termosféra'
    ],
    points: 1
  }, {
    id: 6,
    type: 'multiple',
    question: 'Jaká je podle mezinárodní standardní atmosféry hmotnost krychle vzduchu o hraně 1 m?',
    correct_answer: '1.225 kg',
    incorrect_answers: [
      '0.01225 kg',
      '0.1225 kg',
      '12.25 kg'
    ],
    points: 1
  },
  {
    id: 7,
    type: 'multiple',
    question: 'Jak se podle mezinárodní standardní atmosféry mění v troposféře teplota s narůstající výškou?',
    correct_answer: 'pokles o 2 C / 1000 ft',
    incorrect_answers: [
      'nárůst o 2 C / 100 m',
      'pokles o 2 C / 100 m',
      'nárůst o 2 C / 1000 ft'
    ],
    points: 1
  },
  {
    id: 8,
    type: 'multiple',
    question: 'Jaká je střední výška tropopauzy podle mezinárodní standardní atmosféry?',
    correct_answer: '11.000 ft',
    incorrect_answers: [
      '11.000 m',
      '36.000 m',
      '18.000 ft'
    ],
    points: 1
  }, {
    id: 9,
    type: 'multiple',
    question: 'Pojem tropopauza je definován jako:',
    correct_answer: 'hraniční vrstva mezi troposférou a stratosférou',
    incorrect_answers: [
      'vrstva nad troposférou, kde dochází k nárůstu teploty',
      'hraniční vrstva mezi mezosférou a stratosférou',
      'výška, nad kterou se teplota začíná snižovat'
    ],
    points: 1
  }, {
    id: 10,
    type: 'multiple',
    question: 'Jaká jednotka se používá pro teploty udávané leteckými meteorologickými službami v Evropě a Africe?',
    correct_answer: 'stupně Celsia ( C)',
    incorrect_answers: [
      'Kelvin',
      'stupně Fahrenheita',
      'Gpdam'
    ],
    points: 1
  }, {
    id: 11,
    type: 'multiple',
    question: 'Co znamená výraz inverzní vrstva?',
    correct_answer: 'vrstva v atmosféře, kde teplota roste s rostoucí výškou',
    incorrect_answers: [
      'vrstva v atmosféře, kde teplota klesá s rostoucí výškou',
      'vrstva v atmosféře, kde je teplota konstantní s rostoucí výškou',
      'hraniční vrstva mezi dvěma jinými vrstvami atmosféry'
    ],
    points: 1
  }, {
    id: 13,
    type: 'multiple',
    question: 'Co znamená výraz izotermální vrstva?',
    correct_answer: 'vrstva v atmosféře, kde je teplota s rostoucí výškou konstantní',
    incorrect_answers: [
      'vrstva v atmosféře, kde teplota roste s rostoucí výškou',
      'vrstva v atmosféře, kde teplota klesá s rostoucí výškou',
      'hraniční vrstva mezi dvěma jinými vrstvami atmosféry'
    ],
    points: 1
  }, {
    id: 14,
    type: 'multiple',
    question: 'Jaký proces může vést ke vzniku inverzní vrstvy ve výšce přibližně 5000 ft (1500 m)?',
    correct_answer: 'chladnutí země vyzařováním během noci',
    incorrect_answers: [
      'rozlévající se sestupný proud vzduchu v oblasti vysokého tlaku vzduchu',
      'intenzivní ohřev sluncem během teplého letního dne',
      'advekce studeného vzduchu v horní troposféře'
    ],
    points: 1
  }, {
    id: 15,
    type: 'multiple',
    question: 'Přizemní inverzní vrstva může být způsobena:',
    correct_answer: 'chladnutím země v noci',
    incorrect_answers: [
      'nárůstem oblačnosti ve středních výškách',
      'rozsáhlým stoupáním vzduchu',
      'zesilujícím nárazovým větrem'
    ],
    points: 1
  }, {
    id: 16,
    type: 'multiple',
    question: 'Jaký je tlak vzduchu ve FL 180 (5500 m) podle mezinárodní standardní atmosféry?',
    correct_answer: '500 hPa',
    incorrect_answers: [
      '1013.25 hPa',
      '250 hPa',
      '300 hPa'
    ],
    points: 1
  }, {
    id: 17,
    type: 'multiple',
    question: 'Tlak měřený pozemní stanicí a přepočítaný na střední hladinu moře (MSL) ve skutečné atmosféře se nazývá:',
    correct_answer: 'QFF',
    incorrect_answers: [
      'QNH',
      'QFE',
      'QNE'
    ],
    points: 1
  }, {
    id: 18,
    type: 'multiple',
    question: 'Co způsobuje pokles hustoty vzduchu?',
    correct_answer: 'nárůst teploty, pokles tlaku',
    incorrect_answers: [
      'nárůst teploty, nárůst tlaku',
      'pokles teploty, pokles tlaku',
      'pokles teploty, nárůst tlaku'
    ],
    points: 1
  }, {
    id: 19,
    type: 'multiple',
    question: 'Tlak na hladině moře podle mezinárodní standardní atmosféry je:',
    correct_answer: '1013.25 hPa',
    incorrect_answers: [
      '113.25 hPa',
      '15 hPa',
      '1123 hPa'
    ],
    points: 1
  }, {
    id: 20,
    type: 'multiple',
    question: 'Výška tropopauzy podle mezinárodní standardní atmosféry je:',
    correct_answer: '36.000 ft',
    incorrect_answers: [
      '11.000 ft',
      '5.500 ft',
      '48.000 ft'
    ],
    points: 1
  }, {
    id: 21,
    type: 'multiple',
    question: 'Barometrický výškoměr ukazuje výšku nad:',
    correct_answer: 'zvolenou referenční tlakovou hladinou',
    incorrect_answers: [
      'střední hladinou moře',
      'hladinou standardního tlaku 1013.25 hPa',
      'zemí'
    ],
    points: 1
  }, {
    id: 22,
    type: 'multiple',
    question: 'Výškoměr lze přezkoušet na zemi nastavením:',
    correct_answer: 'QNH a porovnáním indikace s nadmořskou výškou letiště',
    incorrect_answers: [
      'QFF a porovnáním indikace s nadmořskou výškou letiště',
      'QFE a porovnáním indikace s nadmořskou výškou letiště',
      'QNE a kontrolou, že ukazuje 0'
    ],
    points: 1
  }, {
    id: 23,
    type: 'multiple',
    question: 'Barometrický výškoměr nastavený na QFE ukazuje:',
    correct_answer: 'výšku nad úrovní tlaku vztaženého k výšce letiště nad mořem',
    incorrect_answers: [
      'výšku nad standardní tlakovou hladinou 1013.25 hPa',
      'skutečnou nadmořskou výšku nad střední hladinou moře (MSL)',
      'výšku nad stření hladinou moře (MSL)'
    ],
    points: 1
  }, {
    id: 24,
    type: 'multiple',
    question: 'Barometrický výškoměr nastavený na QNH ukazuje:',
    correct_answer: 'výšku nad střední hladinou moře (MSL)',
    incorrect_answers: [
      'výšku nad hladinou standardního tlaku 1013.25 hPa',
      'výšku nad tlakovou hladinou nadmořské výšky letiště',
      'skutečnou nadmořskou výšku nad tlakovou hladinou střední hladiny moře (MSL)'
    ],
    points: 1
  }, {
    id: 25,
    type: 'multiple',
    question: 'Jaká je skutečná nadmořská výška zaokrouhlená na nejbližších 50 ft, je-li dáno: QNH: 983 hPa,FL 85, teplota vnějšího vzduchu: ISA - 10',
    correct_answer: '7300 ft',
    incorrect_answers: [
      '7600 ft',
      '9400 ft',
      '7900 ft'
    ],
    points: 1
  }, {
    id: 26,
    type: 'multiple',
    question: 'Jak lze z přízemních meteorologických map odvodit směr a rychlost větru?',
    correct_answer: 'podle zaoblení a vzdálenosti izobar',
    incorrect_answers: [
      'podle tvaru a vzdálenosti izohyps',
      'podle tvaru teplých a studených front',
      'z údajů v textové části mapy'
    ],
    points: 1
  }, {
    id: 27,
    type: 'multiple',
    question: 'Jaká síla způsobuje vítr?',
    correct_answer: 'síla tlakového gradientu',
    incorrect_answers: [
      'Coriolisova síla',
      'odstředivá síla',
      'termální síla'
    ],
    points: 1
  }, {
    id: 28,
    type: 'multiple',
    question: 'Směr větru nad třecí vrstvou s převažujícím tlakovým gradientem je:',
    correct_answer: 'rovnoběžně s izobarami',
    incorrect_answers: [
      'kolmo na izobary',
      'pod úhlem 30 k izobarám směrem k tlakové níži',
      'kolmo na izohypsy'
    ],
    points: 1
  }, {
    id: 29,
    type: 'multiple',
    question: 'Který z uvedených povrchů nejvíc sníží rychlost větru třením?',
    correct_answer: 'horský terén pokrytý vegetací',
    incorrect_answers: [
      'rovina, množství vegetace',
      'rovina, poušť, žádná vegetace',
      'oceán'
    ],
    points: 1
  }, {
    id: 30,
    type: 'multiple',
    question: 'Pohyb vzduchu směrem k sobě se nazývá:',
    correct_answer: 'konvergence',
    incorrect_answers: [
      'divergence',
      'sankordence',
      'subsidence'
    ],
    points: 1
  }, {
    id: 31,
    type: 'multiple',
    question: 'Pohyb vzduchu směrem od sebe se nazývá:',
    correct_answer: 'divergence',
    incorrect_answers: [
      'subsidence',
      'konvergence',
      'sankordence'
    ],
    points: 1
  }, {
    id: 32,
    type: 'multiple',
    question: 'Jaký vývoj počasí způsobí přízemní konvergence?',
    correct_answer: 'stoupající vzduch a tvorba oblačnosti',
    incorrect_answers: [
      'klesající vzduch a rozpouštění oblačnosti',
      'klesající vzduch a tvorba oblačnosti',
      'stoupající vzduch a rozpouštění oblačnosti'
    ],
    points: 1
  }, {
    id: 33,
    type: 'multiple',
    question: ' Když se čelně střetnou dvě vzduchové masy, jak se to nazývá a jaký jev bude následovat?',
    correct_answer: 'konvergence se stoupajícím vzduchem',
    incorrect_answers: [
      'divergence se stoupajícím vzduchem',
      'konvergence s klesajícím vzduchem',
      'divergence s klesajícím vzduchem'
    ],
    points: 1
  }, {
    id: 34,
    type: 'multiple',
    question: 'Jaké vzduchové masy hlavně ovlivňují střední Evropu?',
    correct_answer: 'studený polární a teplý subtropický vzduch',
    incorrect_answers: [
      'rovníkový a tropický teplý vzduch',
      'arktický a polární studený vzduch',
      'tropický a arktický studený vzduch'
    ],
    points: 1
  }, {
    id: 35,
    type: 'multiple',
    question: 'S ohledem globální cirkulaci v atmosféře, kde se setkává studený polární a teplý subtropický vzduch?',
    correct_answer: 'na polární frontě',
    incorrect_answers: [
      'na zeměpisných pólech',
      'na rovníku',
      'v subtropickém pásu vyššího tlaku vzduchu'
    ],
    points: 1
  }, {
    id: 36,
    type: 'multiple',
    question: 'Vítr vanoucí do kopce je definován jako:',
    correct_answer: 'anabatický vítr',
    incorrect_answers: [
      'katabatický vítr',
      'konvergentní vítr',
      'subsidentní vítr'
    ],
    points: 1
  }, {
    id: 37,
    type: 'multiple',
    question: 'Vítr vanoucí směrem dolů z kopce se nazývá:',
    correct_answer: 'katabatický vítr',
    incorrect_answers: [
      'anabatický vítr',
      'konvergentní vítr',
      'subsidentní vítr'
    ],
    points: 1
  }, {
    id: 38,
    type: 'multiple',
    question: 'Vzduch sestupující za pohořím je definován jako:',
    correct_answer: 'katabatický vítr',
    incorrect_answers: [
      'anabatický vítr',
      'konvergentní vítr',
      'divergentní vítr'
    ],
    points: 1
  }, {
    id: 39,
    type: 'multiple',
    question: 'Fén obvykle vzniká při:',
    correct_answer: 'stabilitě, větru vanoucímu proti pohoří',
    incorrect_answers: [
      'instabilitě, větru vanoucímu proti pohoří',
      'instabilitě, oblasti vysokého tlaku vzduchu a za bezvětří',
      'stabilitě, oblasti vysokého tlaku vzduchu a za bezvětří'
    ],
    points: 1
  }, {
    id: 40,
    type: 'multiple',
    question: 'Jaký typ turbulence se typicky vyskytuje v blízkosti země na závětrné straně při fénu?',
    correct_answer: 'rotorové proudění',
    incorrect_answers: [
      'termická turbulence',
      'inverzní turbulence',
      'turbulence v čistém vzduchu'
    ],
    points: 1
  }, {
    id: 41,
    type: 'multiple',
    question: 'Pokles teploty s rostoucí výškou v troposféře je podle mezinárodní standardní atmosféry:',
    correct_answer: '0.65 C / 100 m',
    incorrect_answers: [
      '3 C / 100 m',
      '0.6 C / 100 m',
      '1 C / 100 m'
    ],
    points: 1
  }]
}
