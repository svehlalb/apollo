export default {
  results: [
    [
      {
        id: 1,
        category: 'Animals',
        type: 'multiple',
        difficulty: 'easy',
        question: 'What is the fastest land animal?',
        correct_answer: 'Cheetah',
        incorrect_answers: [
          'Lion',
          'Thomson Gazelle',
          'Pronghorn Antelope'
        ],
        points: null
      },
      {
        id: 2,
        category: 'Animals',
        type: 'boolean',
        difficulty: 'medium',
        question: 'An octopus can fit through any hole larger than its beak.',
        correct_answer: 'True',
        incorrect_answers: [
          'False'
        ],
        points: null
      },
      {
        id: 3,
        category: 'Animals',
        type: 'multiple',
        difficulty: 'easy',
        question: 'Hippocampus is the Latin name for which marine creature?',
        correct_answer: 'Seahorse',
        incorrect_answers: [
          'Dolphin',
          'Whale',
          'Octopus'
        ],
        points: null
      },
      {
        id: 4,
        category: 'Animals',
        type: 'boolean',
        difficulty: 'easy',
        question: 'A flock of crows is known as a homicide.',
        correct_answer: 'False',
        incorrect_answers: [
          'True'
        ],
        points: null
      },
      {
        id: 5,
        category: 'Animals',
        type: 'multiple',
        difficulty: 'hard',
        question: 'What type of creature is a Bonobo?',
        correct_answer: 'Ape',
        incorrect_answers: [
          'Lion',
          'Parrot',
          'Wildcat'
        ],
        points: null
      },
      {
        id: 6,
        category: 'Animals',
        type: 'multiple',
        difficulty: 'medium',
        question: 'What is the fastest animal?',
        correct_answer: 'Peregrine Falcon',
        incorrect_answers: [
          'Golden Eagle',
          'Cheetah',
          'Horsefly'
        ],
        points: null
      },
      {
        id: 7,
        category: 'Animals',
        type: 'multiple',
        difficulty: 'hard',
        question: 'What is the collective noun for vultures?',
        correct_answer: 'Wake',
        incorrect_answers: [
          'Ambush',
          'Building',
          'Gaze'
        ],
        points: null
      }
    ]
  ]
}
