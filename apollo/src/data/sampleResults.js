export default [
  [{
    id: 11,
    name: 'Petr Dub',
    username: 'PetDub',
    variant: 'A',
    max_points: 50,
    points: 24
  },
  {
    id: 12,
    name: 'Alfons Kalousek',
    username: 'Alfousek',
    variant: 'B',
    max_points: 50,
    points: 39
  },
  {
    id: 13,
    name: 'Robert Kalousek',
    username: 'Robousek',
    variant: 'B',
    max_points: 50,
    points: 42
  },
  {
    id: 14,
    name: 'Dominik Kalousek',
    username: 'Domiousek',
    variant: 'B',
    max_points: 50,
    points: 2
  },
  {
    id: 15,
    name: 'Karel Kalousek',
    username: 'Kaousek',
    variant: 'B',
    max_points: 50,
    points: 2
  },
  {
    id: 16,
    name: 'Jana Veselá',
    username: 'Jenvesel',
    variant: 'B',
    max_points: 50,
    points: 2
  },
  {
    id: 17,
    name: 'Veronika Konečná',
    username: 'Veronecna',
    variant: 'B',
    max_points: 50,
    points: 2
  }
  ]
]
