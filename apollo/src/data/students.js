/** __Test data__
 *    Students
 */

export default {
  content: [[
    {
      username: 'jikla',
      firstname: 'Jiří',
      lastname: 'Klapal',
      email: 'jikla@example.com'
    },
    {
      username: 'jakot',
      firstname: 'Jaroslav',
      lastname: 'Kotík',
      email: 'jakot@example.com'
    },
    {
      username: 'emsal',
      firstname: 'Emil',
      lastname: 'Šálek',
      email: 'emsal@example.com'
    },
    {
      username: 'vahab',
      firstname: 'Václav',
      lastname: 'Habrda',
      email: 'vahab@example.com'
    },
    {
      username: 'dahlo',
      firstname: 'Daniel',
      lastname: 'Hloušek',
      email: 'dahlo@example.com'
    },
    {
      username: 'jocvr',
      firstname: 'Josef',
      lastname: 'Cvrček',
      email: 'jocvr@example.com'
    },
    {
      username: 'frrie',
      firstname: 'František',
      lastname: 'Riegger',
      email: 'frrie@example.com'
    },
    {
      username: 'rakot',
      firstname: 'Radek',
      lastname: 'Kotlář',
      email: 'rakot@example.com'
    },
    {
      username: 'mikon',
      firstname: 'Miloš',
      lastname: 'Konvalinka',
      email: 'mikon@example.com'
    },
    {
      username: 'jahru',
      firstname: 'Jakub',
      lastname: 'Hruška',
      email: 'jahru@example.com'
    }/*,
    {
      username: 'romis',
      firstname: 'Roman',
      lastname: 'Miškovský',
      email: 'romis@example.com'
    },
    {
      username: 'minaz',
      firstname: 'Milan',
      lastname: 'Nazaruk',
      email: 'minaz@example.com'
    },
    {
      username: 'ivluk',
      firstname: 'Ivan',
      lastname: 'Lukeš',
      email: 'ivluk@example.com'
    },
    {
      username: 'minov',
      firstname: 'Michal',
      lastname: 'Novotný',
      email: 'minov@example.com'
    },
    {
      username: 'janes',
      firstname: 'Jan',
      lastname: 'Nesládek',
      email: 'janes@example.com'
    },
    {
      username: 'togre',
      firstname: 'Tomáš',
      lastname: 'Grégr',
      email: 'togre@example.com'
    },
    {
      username: 'pehem',
      firstname: 'Petr',
      lastname: 'Hemmr',
      email: 'pehem@example.com'
    },
    {
      username: 'zdhol',
      firstname: 'Zdeněk',
      lastname: 'Holík',
      email: 'zdhol@example.com'
    },
    {
      username: 'lahli',
      firstname: 'Ladislav',
      lastname: 'Hlína',
      email: 'lahli@example.com'
    },
    {
      username: 'pasle',
      firstname: 'Pavel',
      lastname: 'Šlejmar',
      email: 'pasle@example.com'
    },
    {
      username: 'frtru',
      firstname: 'Františka',
      lastname: 'Trunečková',
      email: 'frtru@example.com'
    },
    {
      username: 'bamay',
      firstname: 'Bára',
      lastname: 'Mayerová',
      email: 'bamay@example.com'
    },
    {
      username: 'pepil',
      firstname: 'Petra',
      lastname: 'Pilipenko',
      email: 'pepil@example.com'
    },
    {
      username: 'hebil',
      firstname: 'Helena',
      lastname: 'Bilčíková',
      email: 'hebil@example.com'
    },
    {
      username: 'ancab',
      firstname: 'Anna',
      lastname: 'Cábová',
      email: 'ancab@example.com'
    },
    {
      username: 'bofer',
      firstname: 'Božena',
      lastname: 'Ferencová',
      email: 'bofer@example.com'
    } ,
   {
      username: 'ivpre',
      firstname: 'Ivana',
      lastname: 'Preisnerová',
      email: 'ivpre@example.com'
    },
    {
      username: 'japro',
      firstname: 'Jaroslava',
      lastname: 'Procházková',
      email: 'japro@example.com'
    },
    {
      username: 'masil',
      firstname: 'Marie',
      lastname: 'Šilhanová',
      email: 'masil@example.com'
    },
    {
      username: 'klwog',
      firstname: 'Klára',
      lastname: 'Wögebauerová',
      email: 'klwog@example.com'
    },
    {
      username: 'kafry',
      firstname: 'Karolína',
      lastname: 'Fryčová',
      email: 'kafry@example.com'
    },
    {
      username: 'jipod',
      firstname: 'Jiřina',
      lastname: 'Podroužková',
      email: 'jipod@example.com'
    },
    {
      username: 'ersku',
      firstname: 'Erika',
      lastname: 'Škubicová',
      email: 'ersku@example.com'
    },
    {
      username: 'havid',
      firstname: 'Hana',
      lastname: 'Vidláková',
      email: 'havid@example.com'
    },
    {
      username: 'javan',
      firstname: 'Jana',
      lastname: 'Vaňková',
      email: 'javan@example.com'
    },
    {
      username: 'evrod',
      firstname: 'Eva',
      lastname: 'Röderová',
      email: 'evrod@example.com'
    },
    {
      username: 'aldol',
      firstname: 'Alena',
      lastname: 'Doležalová',
      email: 'aldol@example.com'
    },
    {
      username: 'lepav',
      firstname: 'Lenka',
      lastname: 'Pavlíčková',
      email: 'lepav@example.com'
    },
    {
      username: 'gakrc',
      firstname: 'Gabriela',
      lastname: 'Krchová',
      email: 'gakrc@example.com'
    },
    {
      username: 'lukri',
      firstname: 'Ludmila',
      lastname: 'Křížová',
      email: 'lukri@example.com'
    } */
  ]]
}
