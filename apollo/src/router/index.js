import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import form from '../views/Form.vue'
import editor from '../views/Editor.vue'
import download from '../views/Download.vue'
import upload from '../views/Upload.vue'
import results from '../views/Results.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/form',
    name: 'form',
    component: form
  },
  {
    path: '/editor',
    name: 'editor',
    component: editor
  },
  {
    path: '/download',
    name: 'download',
    component: download
  },
  {
    path: '/upload',
    name: 'upload',
    component: upload
  },
  {
    path: '/results',
    name: 'results',
    component: results
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router
